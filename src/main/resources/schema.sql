CREATE TABLE IF NOT EXISTS trade (
    id INTEGER(11)        NOT NULL AUTO_INCREMENT,
    stock VARCHAR(50) NOT NULL,
    price DOUBLE      NOT NULL,
    volume INTEGER        NOT NULL,
    PRIMARY KEY (id)
);
