package com.citi.training.assessment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.assessment.dao.TradeDao;
import com.citi.training.assessment.model.Trade;

@Component
public class TradeService {

    @Autowired
    TradeDao tradeDao;

    public List<Trade> findAll() {
        return tradeDao.findAll();
    }

    public int create(Trade trade) {
        return tradeDao.create(trade);
    }

    public Trade findById(int id) {
        return tradeDao.findById(id);
    }

    public void deleteById(int id) {
        tradeDao.deleteById(id);
    }
}
