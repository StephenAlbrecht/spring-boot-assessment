package com.citi.training.assessment.dao;

import java.util.List;

import com.citi.training.assessment.model.Trade;

public interface TradeDao {

    List<Trade> findAll();
    int create(Trade trade);
    Trade findById(int id);
    void deleteById(int id);
}
