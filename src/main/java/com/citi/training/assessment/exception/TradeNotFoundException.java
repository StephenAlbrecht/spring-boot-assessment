package com.citi.training.assessment.exception;

@SuppressWarnings("serial")
public class TradeNotFoundException extends RuntimeException {

    public TradeNotFoundException(String msg) {
        super(msg);
    }
}
