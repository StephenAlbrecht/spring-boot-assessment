package com.citi.training.assessment.model;

import org.junit.Test;

import com.citi.training.assessment.model.Trade;

public class TradeTests {

    @Test
    public void test_Trade_fullConstructor() {
        Trade testTrade = new Trade(0, "MSFT", 354.28, 50);
        assert(testTrade.getId() == 0);
        assert(testTrade.getStock().equals("MSFT"));
        assert(testTrade.getPrice() == 354.28);
        assert(testTrade.getVolume() == 50);
    }

    @Test
    public void test_Trade_setters() {
        Trade testTrade = new Trade(0, "MSFT", 354.28, 50);
        testTrade.setId(10);
        testTrade.setStock("GOOGL");
        testTrade.setPrice(1078.99);
        testTrade.setVolume(300);

        assert(testTrade.getId() == 10);
        assert(testTrade.getStock().equals("GOOGL"));
        assert(testTrade.getPrice() == 1078.99);
        assert(testTrade.getVolume() == 300);
    }

    @Test
    public void test_Trade_toString() {
        Trade testTrade = new Trade(0, "MSFT", 354.28, 50);

        assert(testTrade.toString() != null);
        assert(testTrade.toString().contains(testTrade.getStock()));
    }
}
