package com.citi.training.assessment.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.assessment.model.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2test")
public class TradeControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void findAll_returnsList() {
        restTemplate.postForEntity(TradeController.BASE_PATH,
                new Trade(-1, "MSFT", 354.28, 50), Trade.class);
        ResponseEntity<List<Trade>> findAllResponse = restTemplate.exchange(
                TradeController.BASE_PATH,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Trade>>() {});

        assertEquals(findAllResponse.getStatusCode(), HttpStatus.OK);
        assertTrue(findAllResponse.getBody().get(0).getStock().equals("MSFT"));
    }
}
