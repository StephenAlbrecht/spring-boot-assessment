package com.citi.training.assessment.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.assessment.dao.TradeDao;
import com.citi.training.assessment.model.Trade;
import com.citi.training.assessment.service.TradeService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TradeServiceTests {

    @Autowired
    TradeService tradeService;

    @MockBean
    private TradeDao mockTradeDao;

    @Test
    public void test_findAllRuns() {
        List<Trade> testList = new ArrayList<>();
        testList.add(new Trade(1, "MSFT", 354.28, 50));

        when(mockTradeDao.findAll()).thenReturn(testList);
        List<Trade> returnedList = tradeService.findAll();

        verify(mockTradeDao).findAll();
        assertEquals(testList, returnedList);
    }

    @Test
    public void test_findByIdRuns() {
        int id = 1;
        Trade testTrade = new Trade(id, "MSFT", 354.28, 50);

        when(mockTradeDao.findById(id)).thenReturn(testTrade);
        Trade returnedTrade = tradeService.findById(id);

        verify(mockTradeDao).findById(id);
        assertEquals(testTrade, returnedTrade);
    }

    @Test
    public void test_createRuns() {
        int testId = 1;
        Trade trade = new Trade(1, "MSFT", 354.28, 50);

        when(mockTradeDao.create(any(Trade.class))).thenReturn(testId);
        int returnedId = tradeService.create(trade);

        verify(mockTradeDao).create(trade);
        assertEquals(testId, returnedId);
    }

    @Test
    public void test_deleteRuns() {
        int id = 1;

        tradeService.deleteById(id);

        verify(mockTradeDao).deleteById(id);
    }
}
